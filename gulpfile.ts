import { dest, series, src, watch } from 'gulp';
import gulp = require('gulp');
import child = require('child_process');
import browserSync = require('browser-sync');

const cleanifyCSS = './node_modules/cleanify-design/dist/css/cleanify.css';
const cleanifyJS = './node_modules/cleanify-design/dist/js/cleanify.js';
const siteRoot = '_site';

function CopyCSS() {
    return src(cleanifyCSS).pipe(dest('./site/assets/css', { overwrite: true }));
}

function CopyJS() {
    return src(cleanifyJS).pipe(dest('./site/assets/js', { overwrite: true }));
}

function ServeWeb() {
    browserSync.init({
        files: [siteRoot + '/**'],
        port: 4000,
        open: false,
        server: {
            baseDir: siteRoot,
            routes: {
                "/chesspawn": siteRoot
            }
        }
    });

    watch(['site/**', '_config.yml'], Jekyll);
}

function Jekyll() {
    return child.spawn('jekyll', ['build']);
}

function JekyllGitlab() {
    return child.spawn('jekyll', ['build', '-d', 'public/']);
}

function GemsBundle() {
    return child.spawn('bundle', ['install']);
}

gulp.task('gitlab', series(GemsBundle, CopyCSS, CopyJS, JekyllGitlab));
gulp.task('default', series(GemsBundle, CopyCSS, CopyJS, Jekyll, ServeWeb));